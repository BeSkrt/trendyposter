package be.vdab.trendyPoster.mainApp;

import be.vdab.trendyPoster.text.TextAdjuster;

import java.util.Scanner;

/**
 * @author Behlül Savaskurt
 * created on 20/06/2021
 */
public class InfluencerApp {

    private static Scanner keyboard;

    public static void main(String[] args) {

        run();
    }

    private static void run() {

        keyboard = new Scanner(System.in);

        System.out.println("Do you want to make hashtags or mentions?");
        System.out.println("1. Hashtags");
        System.out.println("2. Mentions");

        int option = keyboard.nextInt();

        switch(option) {
            case 1:
                System.out.println("Please enter your keywords:");
                Scanner wordToHashTag = new Scanner(System.in);
                String hashTags = wordToHashTag.nextLine();

                TextAdjuster textAdjuster = new TextAdjuster(hashTags);
                textAdjuster.addAtStartOfEveryWord(s -> String.format("#%s", s));
                wordToHashTag.close();
                break;

            case 2:
                System.out.println("Please enter your mentions:");
                Scanner wordToMention = new Scanner(System.in);
                String mentions = wordToMention.nextLine();

                TextAdjuster textAdjuster2 = new TextAdjuster(mentions);
                textAdjuster2.addAtStartOfEveryWord(s -> String.format("@%s", s));
                wordToMention.close();
                break;

        }

        keyboard.close();
    }
}
