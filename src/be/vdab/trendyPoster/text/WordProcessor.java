package be.vdab.trendyPoster.text;

/**
 * @author Behlül Savaskurt
 * created on 20/06/2021
 */
@FunctionalInterface
public interface WordProcessor {
    String process(String s);
}
