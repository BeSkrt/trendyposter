package be.vdab.trendyPoster.text;

/**
 * @author Behlül Savaskurt
 * created on 20/06/2021
 */
public class TextAdjuster {
    private String text;

    public TextAdjuster(String text) {
        this.text = text;
    }

    public void addAtStartOfEveryWord (WordProcessor wordProcessor) {
        for (String s : text.split(" ")) {
            System.out.println(wordProcessor.process(s));
        }
    }

}
